package com.nicolasrf.drinkshoprdatabaseserver.Utils;

import com.nicolasrf.drinkshoprdatabaseserver.Model.Category;
import com.nicolasrf.drinkshoprdatabaseserver.Model.Drink;
import com.nicolasrf.drinkshoprdatabaseserver.Model.Order;
import com.nicolasrf.drinkshoprdatabaseserver.Retrofit.FCMRetrofitClient;
import com.nicolasrf.drinkshoprdatabaseserver.Retrofit.IFCMService;

import java.util.ArrayList;
import java.util.List;

public class Common {

    public static Category currentCategory;
    public static Drink currentDrink;
    public static Order currentOrder;

    public static List<Category> menuList = new ArrayList<>();

    public static String topicName = "News";


    public static final String BASE_URL = "https://nicolasrf.000webhostapp.com/drinkshop/";
    public static final String FCM_URL = "https://fcm.googleapis.com/";


    public static IFCMService getFCMApi(){
        return FCMRetrofitClient.getClient(FCM_URL).create(IFCMService.class);
    }

    public static String convertCodeToStatus(int orderStatus) {
        switch (orderStatus)    {
            case 0:
                return "Solicitado";
            case 1:
                return "En Proceso";
            case 2:
                return "En Camino";
            case 3:
                return "Finalizado";
            case -1:
                return "Cancelado";

            default:
                return "Order Error";
        }
    }
}
