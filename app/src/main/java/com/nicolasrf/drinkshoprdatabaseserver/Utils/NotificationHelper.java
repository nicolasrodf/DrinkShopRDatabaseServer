package com.nicolasrf.drinkshoprdatabaseserver.Utils;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.net.Uri;
import android.os.Build;

import com.nicolasrf.drinkshoprdatabaseserver.R;


public class NotificationHelper extends ContextWrapper {
    private static final String NICO_DEV_ID = "com.nicolasrf.drinkshopserver.NicoDEV";
    private static final String NICO_APP_NAME = "Drink Shop";

    private NotificationManager notificationManager;


    public NotificationHelper(Context base) {
        super(base);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createChannel();
        }
    }

    @TargetApi(Build.VERSION_CODES.O)
    private void createChannel() {
        NotificationChannel nicoChannel = new NotificationChannel(NICO_DEV_ID,NICO_APP_NAME,
                NotificationManager.IMPORTANCE_DEFAULT);
        nicoChannel.enableLights(false);
        nicoChannel.enableVibration(true);
        nicoChannel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);

        getNotificationManager().createNotificationChannel(nicoChannel);
    }

    public NotificationManager getNotificationManager() {
        if(notificationManager == null)
            notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        return notificationManager;
    }

    @TargetApi(Build.VERSION_CODES.O)
    public Notification.Builder getDrinkShopNotification(String title,
                                                         String message,
                                                         Uri sound, PendingIntent pendingIntent)
    {
        return new Notification.Builder(getApplicationContext(),NICO_DEV_ID)
                .setContentTitle(title)
                .setContentTitle(title)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentText(message)
                .setSound(sound)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);
    }
}
