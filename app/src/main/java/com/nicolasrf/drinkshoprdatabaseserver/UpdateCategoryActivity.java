package com.nicolasrf.drinkshoprdatabaseserver;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.nicolasrf.drinkshoprdatabaseserver.Utils.Common;
import com.nicolasrf.drinkshoprdatabaseserver.Utils.UploadCallBack;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import id.zelory.compressor.Compressor;
import io.reactivex.disposables.CompositeDisposable;

public class UpdateCategoryActivity extends AppCompatActivity implements UploadCallBack {
    private static final String TAG = "UpdateCategoryActivity";
    private static final int PICK_FILE_REQUEST = 9999;

    ImageView browserImageView;
    EditText nameEditText;
    Button updateButton, deleteButton;

    CompositeDisposable compositeDisposable;

    Uri selectedFileUri = null;
    File compressedImageFile;

    //Uri downloadUri;

    ProgressDialog progressDialog;

    private FirebaseDatabase firebaseDatabase;
    private StorageReference storageReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_category);

        firebaseDatabase = FirebaseDatabase.getInstance();
        storageReference = FirebaseStorage.getInstance().getReference();

        //View
        deleteButton = findViewById(R.id.delete_button);
        updateButton = findViewById(R.id.update_button);
        nameEditText = findViewById(R.id.name_edit_text);
        browserImageView = findViewById(R.id.browser_image_view);
        //RxJava
        compositeDisposable = new CompositeDisposable();

        displayData();

        //Event
        browserImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkPermissions();
            }
        });

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateCategory();
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteCategory();
            }
        });

    }

    private void deleteCategory() {


        //delete category in database
        firebaseDatabase.getReference("categories")
                .child(Common.currentCategory.getId())
                .removeValue()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(UpdateCategoryActivity.this, "Menu has been deleted", Toast.LENGTH_SHORT).show();
                        selectedFileUri=null;
                        Common.currentCategory=null;
                        finish();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(TAG, "onFailure: " + e.getMessage());
            }
        });

        //Delete from Storage
        // Create a storage reference from our app
        FirebaseStorage storage = FirebaseStorage.getInstance();
        //StorageReference storageRefFull = storage.getReferenceFromUrl(Common.request_selected.getImageFull());
        StorageReference storageRefUrl = storage.getReferenceFromUrl(Common.currentCategory.getLink());

        // Delete the file url
        storageRefUrl.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                // File deleted successfully
                Log.d(TAG, "onSuccess: DELETED IMAGE URL FROM STORAGE");
                finish(); //finish activity
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Uh-oh, an error occurred!
                Log.d(TAG, "onFailure: ERROR: " + exception.getMessage());
                finish(); //finish activity
            }
        });

    }

    private void updateCategory() {


        if(selectedFileUri!=null){

            //Upload to Storage

            progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading...");
            progressDialog.show();

            //create file
            File file = new File(selectedFileUri.getPath());

            //compress before upload
            try {
                compressedImageFile = new Compressor(UpdateCategoryActivity.this)
                        .setMaxHeight(640)
                        .setMaxWidth(480)
                        .setQuality(70)
                        .compressToFile(file);
            } catch (IOException e) {
                e.printStackTrace();
            }

            String fileName = UUID.randomUUID().toString();

            //create Uri from file (compressed in this case)
            Uri filePath = Uri.fromFile(compressedImageFile);

            //create reference
            final StorageReference riversRef = storageReference.child("images/category/"+fileName+".jpg");
            //create Upload task for put this file from path
            UploadTask uploadTask = riversRef.putFile(filePath);

            //Hasta aqui ya finaliza la subida, las siguientes son validaciones.-

            uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    // taskSnapshot.getMetadata() contains file metadata such as size, content-type, etc.
                    // ...
                    progressDialog.dismiss();
                    Toast.makeText(UpdateCategoryActivity.this, "Uploaded!", Toast.LENGTH_SHORT).show();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    progressDialog.dismiss();
                    Toast.makeText(UpdateCategoryActivity.this, "ERROR: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0*taskSnapshot.getBytesTransferred()/taskSnapshot.getTotalByteCount());
                            progressDialog.setMessage("Uploaded: " + (int)progress+"%");
                        }
                    });

            //GET DOWNLOAD URI
            Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }
                    return riversRef.getDownloadUrl();

                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        Uri downloadUri = task.getResult(); //  URI obtenido desde el urlTask! con el metodo del return anterior!
                        Log.d(TAG, "onComplete: DOWNLOAD URI " + downloadUri);

                        //Update name and link

                        Map<String, Object> childUpdates = new HashMap<>();
                        childUpdates.put("name", nameEditText.getText().toString());
                        childUpdates.put("link", downloadUri.toString());

                        firebaseDatabase.getReference("categories")
                                .child(Common.currentCategory.getId())
                                .updateChildren(childUpdates)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Toast.makeText(UpdateCategoryActivity.this, "Category updated", Toast.LENGTH_SHORT).show();
                                        selectedFileUri=null;
                                        Common.currentCategory=null;
                                        finish();
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.d(TAG, "onFailure: " + e.getMessage());
                            }
                        });

                        //Toast.makeText(HomeActivity.this, "Uri obtenido!", Toast.LENGTH_SHORT).show();
                    } else {
                        // Handle failures
                        // ...
                    }
                }
            });



        } else {
            //Update only name (link remains with Common.currentCategory.getLink() )

            Map<String, Object> childUpdates = new HashMap<>();
            childUpdates.put("name", nameEditText.getText().toString());
            childUpdates.put("link", Common.currentCategory.getLink());

            firebaseDatabase.getReference("categories")
                    .child(Common.currentCategory.getId())
                    .updateChildren(childUpdates)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(UpdateCategoryActivity.this, "Category updated", Toast.LENGTH_SHORT).show();
                            selectedFileUri=null;
                            Common.currentCategory=null;
                            finish();
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.d(TAG, "onFailure: " + e.getMessage());
                }
            });
        }

    }

    //Crtl+O
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                selectedFileUri = result.getUri();
                Picasso.with(this).load(selectedFileUri).into(browserImageView);
                //uploadFileToServer(selectedFileUri);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {

                Exception error = result.getError();
                Toast.makeText(UpdateCategoryActivity.this, "Error crop image.", Toast.LENGTH_SHORT).show();
            }
        }

    }

    private void initImageCropper() {
        //Set crop properties.
        CropImage.activity()
                .setInitialCropWindowPaddingRatio(0)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMinCropResultSize(512, 512)
                .setActivityTitle("RECORTAR")
                .setCropMenuCropButtonTitle("OK")
                .start(this);
    }

    public void checkPermissions(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (ContextCompat.checkSelfPermission(UpdateCategoryActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                Toast.makeText(UpdateCategoryActivity.this, "Permiso denegado", Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(UpdateCategoryActivity.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            } else {
                initImageCropper();
            }
        } else {
            initImageCropper();
        }
    }


    private void displayData() {
        if(Common.currentCategory != null){
            Picasso.with(this)
                    .load(Common.currentCategory.getLink())
                    .into(browserImageView);

            nameEditText.setText(Common.currentCategory.getName());
        }
    }

    //Ctrol+O

    @Override
    protected void onDestroy() {
        dismissProgressDialog();
        compositeDisposable.clear();
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        compositeDisposable.clear();
        super.onStop();
    }

    @Override
    public void onProgressUpdate(int percentage) {

    }

    private void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

}
