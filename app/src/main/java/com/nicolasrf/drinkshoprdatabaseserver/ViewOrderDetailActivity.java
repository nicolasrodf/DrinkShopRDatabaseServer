package com.nicolasrf.drinkshoprdatabaseserver;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.nicolasrf.drinkshoprdatabaseserver.Adapter.OrderDetailAdapter;
import com.nicolasrf.drinkshoprdatabaseserver.Model.DataMessage;
import com.nicolasrf.drinkshoprdatabaseserver.Model.MyResponse;
import com.nicolasrf.drinkshoprdatabaseserver.Model.Order;
import com.nicolasrf.drinkshoprdatabaseserver.Model.Token;
import com.nicolasrf.drinkshoprdatabaseserver.Model.User;
import com.nicolasrf.drinkshoprdatabaseserver.Retrofit.IFCMService;
import com.nicolasrf.drinkshoprdatabaseserver.Utils.Common;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewOrderDetailActivity extends AppCompatActivity {
    private static final String TAG = "ViewOrderDetailActivity";

    TextView txt_order_id, txt_name, txt_phone, txt_order_price, txt_order_address, txt_order_comment;
    Spinner orderStatusSpinner;

    RecyclerView orderRecyclerView;

    //Declare values for spinner
    String[] spinnerSource = new String[]{
            "Cancelado", //index 0
            "Solicitado", //1
            "En Proceso", //2
            "En Camino", //3
            "Finalizado", //4
    };

    IFCMService mFCMService;

    CompositeDisposable compositeDisposable = new CompositeDisposable();

    String name;

    ImageView messageImageView, phoneImageView;

    FirebaseDatabase firebaseDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);

        setTitle("READY BEBIDAS");

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        firebaseDatabase = FirebaseDatabase.getInstance();

        mFCMService = Common.getFCMApi();

        //Send whatsapp msg
        messageImageView = findViewById(R.id.msg_image_view);
        messageImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "This is my text to send.");
                sendIntent.setType("text/plain");
                sendIntent.setPackage("com.whatsapp");
                startActivity(sendIntent);
            }
        });

        //Make phonecall
        phoneImageView = findViewById(R.id.phone_image_view);
        phoneImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(ViewOrderDetailActivity.this, "clicked", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", Common.currentOrder.getUserPhone(), null));
                startActivity(intent);
            }
        });

        txt_order_id = findViewById(R.id.order_id_text_view);
        txt_name = findViewById(R.id.name_text_view);
        txt_phone = findViewById(R.id.phone_text_view);
        txt_order_price = findViewById(R.id.order_price_text_view);
        txt_order_address = findViewById(R.id.order_address_text_view);
        txt_order_comment = findViewById(R.id.order_comment_text_view);
        orderStatusSpinner = findViewById(R.id.order_status_spinner);

        orderRecyclerView = findViewById(R.id.recycler_order_detail);
        orderRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        orderRecyclerView.setAdapter(new OrderDetailAdapter(this));

        firebaseDatabase.getReference("users")
                .child(Common.currentOrder.getUserPhone())
                .child("information")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists()){
                            User user = dataSnapshot.getValue(User.class);
                            name = user.getName();
                            txt_name.setText(name);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });


        txt_order_id.setText(new StringBuilder("#").append(Common.currentOrder.getId()));
        txt_phone.setText(Common.currentOrder.getUserPhone());
        txt_order_price.setText(new StringBuilder("$ ").append(Common.currentOrder.getOrderPrice()));
        txt_order_address.setText(Common.currentOrder.getOrderAddress());
        txt_order_comment.setText(Common.currentOrder.getOrderComment());



        //Set Array adapter for order status Spinner (for place data)
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item,
                spinnerSource);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        orderStatusSpinner.setAdapter(spinnerArrayAdapter);

        setSpinnerSelectedBaseOnOrderStatus();



    }

    private void setSpinnerSelectedBaseOnOrderStatus() {

        switch (Common.currentOrder.getOrderStatus())
        {
            case -1:
                orderStatusSpinner.setSelection(0); //Cancelled
                break;
            case 0:
                orderStatusSpinner.setSelection(1); //Placed
                break;
            case 1:
                orderStatusSpinner.setSelection(2); //Processed
                break;
            case 2:
                orderStatusSpinner.setSelection(3); //Shipping
                break;
            case 3:
                orderStatusSpinner.setSelection(4); //Shipped
                break;
        }
    }

    //Ctrol+O
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_order_detail,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.action_save_order_detail)
            saveUpdateOrder();
        return true;
    }

    private void saveUpdateOrder() {

        final int order_status = orderStatusSpinner.getSelectedItemPosition()-1;

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("orderStatus", order_status);

        firebaseDatabase.getReference("orders")
                .child(Common.currentOrder.getId())
                .updateChildren(childUpdates)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(ViewOrderDetailActivity.this, "Order status updated!", Toast.LENGTH_SHORT).show();
                        sendOrderUpdateNotification(Common.currentOrder, order_status);
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(ViewOrderDetailActivity.this, "Error de conexion", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void sendOrderUpdateNotification(final Order currentOrder, final int order_status) {

        firebaseDatabase.getReference("tokens")
                .child(Common.currentOrder.getUserPhone())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            Token token = dataSnapshot.getValue(Token.class);
                            String userToken = token.getToken();

                            //
                            DataMessage dataMessage = new DataMessage();
                            Map<String, String> contentSend = new HashMap<>();
                            contentSend.put("title", "Tu orden tiene un nuevo status:");
                            contentSend.put("message", "Orden #: " + currentOrder.getId() + " ha sido actualizada a " +
                                    Common.convertCodeToStatus(order_status));
                            //Toast.makeText(ViewOrderDetailActivity.this, "TOEKN; " + userToken.getToken(), Toast.LENGTH_SHORT).show();
                            dataMessage.setTo(userToken);
                            dataMessage.setData(contentSend);
                            mFCMService.sendNotification(dataMessage)
                                    .enqueue(new Callback<MyResponse>() {
                                        @Override
                                        public void onResponse(Call<MyResponse> call, Response<MyResponse> response) {
                                            Log.d(TAG, "onResponse: CODE " + response.code());
                                            if (response.code() == 200) {
                                                Log.d(TAG, "onResponse: SUCCESS  " + response.body().success);
                                                if (response.body().success == 1) {
                                                    Toast.makeText(ViewOrderDetailActivity.this, "Order updated!!", Toast.LENGTH_SHORT).show();
                                                    finish();
                                                } else {
                                                    Log.d(TAG, "onResponse: SEND FAILED");
                                                    Toast.makeText(ViewOrderDetailActivity.this, "Send notification failed !", Toast.LENGTH_SHORT).show();
                                                }
                                            }

                                        }

                                        @Override
                                        public void onFailure(Call<MyResponse> call, Throwable t) {
                                            Toast.makeText(ViewOrderDetailActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                    });

                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }

                });


    }

    @Override
    protected void onStop() {
        compositeDisposable.clear();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        compositeDisposable.clear();
        super.onDestroy();
    }
}
