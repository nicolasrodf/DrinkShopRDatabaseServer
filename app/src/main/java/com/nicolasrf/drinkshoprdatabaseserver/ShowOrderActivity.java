package com.nicolasrf.drinkshoprdatabaseserver;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.nicolasrf.drinkshoprdatabaseserver.Adapter.OrderViewAdapter;
import com.nicolasrf.drinkshoprdatabaseserver.Model.Order;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

public class ShowOrderActivity extends AppCompatActivity {
    private static final String TAG = "ShowOrderActivity";

    RecyclerView recyclerView;

    CompositeDisposable compositeDisposable = new CompositeDisposable();

    BottomNavigationView navigationView;

    private FirebaseDatabase firebaseDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_order);

        firebaseDatabase = FirebaseDatabase.getInstance();

        recyclerView = findViewById(R.id.recycler_orders);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        navigationView = findViewById(R.id.bottom_navigation);

        navigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                if (item.getItemId() == R.id.order_new) {
                    loadAllOrders(0);
                }
                else if (item.getItemId() == R.id.order_cancel) {
                    loadAllOrders(-1);
                }
                else if (item.getItemId() == R.id.order_processing) {
                    loadAllOrders(1);
                }
                else if (item.getItemId() == R.id.order_shipping) {
                    loadAllOrders(2);
                }
                else if (item.getItemId() == R.id.order_shipped) {
                    loadAllOrders(3);
                }
                return true;
            }
        });
        loadAllOrders(0);
    }

    private void loadAllOrders(int statusCode) {

        final List<Order> orders = new ArrayList<>();

        firebaseDatabase.getReference("orders")
                .orderByChild("orderStatus")
                .equalTo(statusCode)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                            Order order = snapshot.getValue(Order.class);
                            orders.add(order);
                        }

                        Toast.makeText(ShowOrderActivity.this, "orders size " + orders.size(), Toast.LENGTH_SHORT).show();
                        displayOrders(orders);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

    }

    private void displayOrders(List<Order> orders) {
        Collections.reverse(orders); //show order for newest first
        OrderViewAdapter adapter = new OrderViewAdapter(this,orders);
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadAllOrders(0);
    }

    @Override
    protected void onStop() {
        compositeDisposable.clear();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        compositeDisposable.clear();
        super.onDestroy();
    }
}
