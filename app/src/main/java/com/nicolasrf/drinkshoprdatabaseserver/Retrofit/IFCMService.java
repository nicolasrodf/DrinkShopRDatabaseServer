package com.nicolasrf.drinkshoprdatabaseserver.Retrofit;

import com.nicolasrf.drinkshoprdatabaseserver.Model.DataMessage;
import com.nicolasrf.drinkshoprdatabaseserver.Model.MyResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface IFCMService {
    @Headers({
            "Content-type:application/json",
            "Authorization:key=AAAAL9Jw29I:APA91bFXTBcEtQLE8iDt_x5RXgBpBvbd7CfVqIP5HfwOXrwroJRGBcO2P-p3SVFxBwx6MsNcHNnt2ErQL5hnr5i0YyBz4sHHy7TPrJZW2pDxlbWQd2e4A3RQQ9IEBiGBdVWYxEBAGlSd"
    })
    @POST("fcm/send")
    Call<MyResponse> sendNotification(@Body DataMessage body);
}
