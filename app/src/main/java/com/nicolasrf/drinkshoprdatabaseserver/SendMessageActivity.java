package com.nicolasrf.drinkshoprdatabaseserver;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.nicolasrf.drinkshoprdatabaseserver.Model.DataMessage;
import com.nicolasrf.drinkshoprdatabaseserver.Model.MyResponse;
import com.nicolasrf.drinkshoprdatabaseserver.Retrofit.IFCMService;
import com.nicolasrf.drinkshoprdatabaseserver.Utils.Common;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SendMessageActivity extends AppCompatActivity {

    MaterialEditText titleEditText, messageEditText;
    Button sendButton;

    IFCMService mFCMService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_message);

        setTitle("ENVIAR MENSAJE DESDE EL APP");

        titleEditText = findViewById(R.id.title_edit_text);
        messageEditText = findViewById(R.id.message_edit_text);
        sendButton = findViewById(R.id.send_message_button);

        mFCMService = Common.getFCMApi();

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String title = new StringBuilder("TAG ").append(titleEditText.getText()).toString(); //Agrego TAG (puede ser cualquier String) para chequear en OrderApp si es para todos o para ese dispositivo.

                Map<String,String> dataSend = new HashMap<>();
                dataSend.put("title", title );
                dataSend.put("message", messageEditText.getText().toString());
                String to = new StringBuilder("/topics/").append(Common.topicName).toString();
                DataMessage dataMessage = new DataMessage(to, dataSend);

                mFCMService.sendNotification(dataMessage)
                        .enqueue(new Callback<MyResponse>() {
                            @Override
                            public void onResponse(Call<MyResponse> call, Response<MyResponse> response) {
                                if (response.code() == 200) {
                                    if (response.body().success == 1) {
                                        Toast.makeText(SendMessageActivity.this, "Error al enviar la notificacion", Toast.LENGTH_SHORT).show();
                                        finish();

                                    } else {
                                        Toast.makeText(SendMessageActivity.this, "Mensaje enviado!", Toast.LENGTH_SHORT).show();
                                        finish();
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<MyResponse> call, Throwable t) {
                                Log.d("TAG", "onFailure: " + t.getMessage());
                            }
                        });

            }
        });
    }
}

