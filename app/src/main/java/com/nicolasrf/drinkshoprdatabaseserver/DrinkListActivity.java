package com.nicolasrf.drinkshoprdatabaseserver;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.nicolasrf.drinkshoprdatabaseserver.Adapter.DrinkListAdapter;
import com.nicolasrf.drinkshoprdatabaseserver.Model.Drink;
import com.nicolasrf.drinkshoprdatabaseserver.Utils.Common;
import com.nicolasrf.drinkshoprdatabaseserver.Utils.UploadCallBack;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import id.zelory.compressor.Compressor;
import io.reactivex.disposables.CompositeDisposable;

public class DrinkListActivity extends AppCompatActivity implements UploadCallBack {
    private static final String TAG = "DrinkListActivity";

    private static final int PICK_FILE_REQUEST = 9998;

    RecyclerView drinksRecycler;

    FloatingActionButton addButton;

    ImageView browserImageView;
    EditText drinkNameEditText, drinkPriceEditText, descriptionEditText;

    CompositeDisposable compositeDisposable = new CompositeDisposable();

    Uri selectedFileUri = null;
    String uploadedImgPath = "";
    File compressedImageFile;

    private FirebaseDatabase firebaseDatabase;
    private StorageReference storageReference;


    //Uri downloadUri;

    String lastKey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drink_list);

        firebaseDatabase = FirebaseDatabase.getInstance();
        storageReference = FirebaseStorage.getInstance().getReference();

        addButton = findViewById(R.id.add_button);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddDrinkDialog();
            }
        });

        drinksRecycler = findViewById(R.id.drinks_recycler);
        drinksRecycler.setLayoutManager(new GridLayoutManager(this,2));
        drinksRecycler.setHasFixedSize(true);

        loadDrinkList(Common.currentCategory.getId());

        getLastKey();
    }

    private void getLastKey() {

        Query query = FirebaseDatabase.getInstance().getReference("drinks").orderByKey().limitToLast(1);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        lastKey = child.getKey();
                        Toast.makeText(DrinkListActivity.this, "LAST KEY: " + lastKey, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    lastKey = "0";
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void showAddDrinkDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Agregar Nuevo Producto");

        View view = LayoutInflater.from(this).inflate(R.layout.add_new_product_layout,null);

        drinkNameEditText = view.findViewById(R.id.drink_name_edit_text);
        drinkPriceEditText = view.findViewById(R.id.drink_price_edit_text);
        descriptionEditText = view.findViewById(R.id.drink_desc_edit_text);
        browserImageView = view.findViewById(R.id.browser_image_view);

        //Event
        browserImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkPermissions();
            }
        });

        //Set View
        builder.setView(view);
        builder.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                uploadedImgPath = "";
                selectedFileUri = null;
            }
        }).setPositiveButton("AGREGAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                if(drinkNameEditText.getText().toString().isEmpty()){
                    Toast.makeText(DrinkListActivity.this, "Por favor ingrese el nombre del producto", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(drinkPriceEditText.getText().toString().isEmpty()){
                    Toast.makeText(DrinkListActivity.this, "Por favor ingrese el precio del producto", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(descriptionEditText.getText().toString().isEmpty()){
                    Toast.makeText(DrinkListActivity.this, "Por favor ingrese la descripcion del producto", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(selectedFileUri.toString().isEmpty()){
                    Toast.makeText(DrinkListActivity.this, "Por favor seleccione la imagen del producto", Toast.LENGTH_SHORT).show();
                    return;
                }


                uploadToServerAndDatabase();



            }
        }).show();

    }

    public void checkPermissions(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (ContextCompat.checkSelfPermission(DrinkListActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                Toast.makeText(DrinkListActivity.this, "Permiso denegado", Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(DrinkListActivity.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            } else {
                initImageCropper();
            }
        } else {
            initImageCropper();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                selectedFileUri = result.getUri();
                Picasso.with(this).load(selectedFileUri).into(browserImageView);
                //uploadFileToServer(selectedFileUri);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {

                Exception error = result.getError();
                Toast.makeText(DrinkListActivity.this, "Error crop image.", Toast.LENGTH_SHORT).show();
            }
        }

    }


    private void initImageCropper() {
        //Set crop properties.
        CropImage.activity()
                .setInitialCropWindowPaddingRatio(0)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMinCropResultSize(512, 512)
                .setActivityTitle("RECORTAR")
                .setCropMenuCropButtonTitle("OK")
                .start(this);
    }

    private void uploadToServerAndDatabase() {

        if(selectedFileUri != null){

            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading...");
            progressDialog.show();

            //create file
            File file = new File(selectedFileUri.getPath());

            //compress before upload
            try {
                compressedImageFile = new Compressor(DrinkListActivity.this)
                        .setMaxHeight(640)
                        .setMaxWidth(480)
                        .setQuality(70)
                        .compressToFile(file);
            } catch (IOException e) {
                e.printStackTrace();
            }

            String fileName = UUID.randomUUID().toString();

            //create Uri from file (compressed in this case)
            Uri filePath = Uri.fromFile(compressedImageFile);

            //create reference
            final StorageReference riversRef = storageReference.child("images/product/"+fileName+".jpg");
            //create Upload task for put this file from path
            UploadTask uploadTask = riversRef.putFile(filePath);

            //Hasta aqui ya finaliza la subida, las siguientes son validaciones.-

            uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    // taskSnapshot.getMetadata() contains file metadata such as size, content-type, etc.
                    // ...
                    Toast.makeText(DrinkListActivity.this, "Uploaded!", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    progressDialog.dismiss();
                    Toast.makeText(DrinkListActivity.this, "ERROR: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0*taskSnapshot.getBytesTransferred()/taskSnapshot.getTotalByteCount());
                            progressDialog.setMessage("Uploaded: " + (int)progress+"%");
                        }
                    });

            //GET DOWNLOAD URI
            Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }
                    return riversRef.getDownloadUrl();

                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        Uri downloadUri = task.getResult(); //  URI obtenido desde el urlTask! con el metodo del return anterior!
                        Log.d(TAG, "onComplete: DOWNLOAD URI " + downloadUri);

                        final Map<String, Object> drink = new HashMap<>();
                        int key = Integer.valueOf(lastKey)+1;
                        drink.put("id", String.valueOf(key));
                        drink.put("name", drinkNameEditText.getText().toString());
                        drink.put("price", Double.parseDouble(drinkPriceEditText.getText().toString()));
                        drink.put("menuId", Common.currentCategory.getId());
                        drink.put("link", downloadUri.toString());
                        drink.put("description", descriptionEditText.getText().toString());


                        //
                        firebaseDatabase.getReference("drinks")
                                .child(String.valueOf(key))
                                .setValue(drink)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Toast.makeText(DrinkListActivity.this, "Drink agregado", Toast.LENGTH_SHORT).show();
                                        loadDrinkList(Common.currentCategory.getId()); //refresh page
                                        selectedFileUri = null;
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(DrinkListActivity.this, "Failed: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });

                        //Toast.makeText(HomeActivity.this, "Uri obtenido!", Toast.LENGTH_SHORT).show();
                    } else {
                        // Handle failures
                        // ...
                    }
                }
            });

        }
    }

    private void loadDrinkList(String menuId) {

        final List<Drink> drinks = new ArrayList<>();

        Query query = firebaseDatabase.getReference("drinks").orderByChild("menuId").equalTo(menuId);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                    Drink drink = snapshot.getValue(Drink.class);
                    drinks.add(drink);
                }

                displayDrinkList(drinks);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void displayDrinkList(List<Drink> drinks) {
        Toast.makeText(this, "SIZE DRINK LIST " +drinks.size(), Toast.LENGTH_SHORT).show();
        DrinkListAdapter adapter = new DrinkListAdapter(this,drinks);
        drinksRecycler.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        loadDrinkList(Common.currentCategory.getId());
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        compositeDisposable.clear();
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        compositeDisposable.clear();
        super.onStop();
    }

    @Override
    public void onProgressUpdate(int percentage) {

    }
}
