package com.nicolasrf.drinkshoprdatabaseserver.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.nicolasrf.drinkshoprdatabaseserver.Adapter.ViewHolder.OrderViewHolder;
import com.nicolasrf.drinkshoprdatabaseserver.Interface.IItemClickListener;
import com.nicolasrf.drinkshoprdatabaseserver.Model.Order;
import com.nicolasrf.drinkshoprdatabaseserver.R;
import com.nicolasrf.drinkshoprdatabaseserver.Utils.Common;
import com.nicolasrf.drinkshoprdatabaseserver.ViewOrderDetailActivity;

import java.util.List;

public class OrderViewAdapter extends RecyclerView.Adapter<OrderViewHolder> {

    private Context context;
    private List<Order> orderList;

    public OrderViewAdapter(Context context, List<Order> orderList) {
        this.context = context;
        this.orderList = orderList;
    }

    @NonNull
    @Override
    public OrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.order_layout,parent,false);

        return new OrderViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderViewHolder holder, final int position) {

        holder.txt_order_id.setText(new StringBuilder("#").append(orderList.get(position).getId()));
        holder.txt_order_price.setText(new StringBuilder("$ ").append(orderList.get(position).getOrderPrice()));
        holder.txt_order_address.setText(orderList.get(position).getOrderAddress());
        holder.txt_order_comment.setText(orderList.get(position).getOrderComment());
        holder.txt_order_status.setText(new StringBuilder("Estado de la Orden : ").append(Common.convertCodeToStatus(orderList.get(position).getOrderStatus())));

        holder.setItemClickListener(new IItemClickListener() {
            @Override
            public void onClick(View view, boolean isLongClick) {
                Common.currentOrder = orderList.get(position);
                context.startActivity(new Intent(context, ViewOrderDetailActivity.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }
}