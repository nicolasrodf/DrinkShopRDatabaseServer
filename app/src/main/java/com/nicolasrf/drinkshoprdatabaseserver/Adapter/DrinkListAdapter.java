package com.nicolasrf.drinkshoprdatabaseserver.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.nicolasrf.drinkshoprdatabaseserver.Adapter.ViewHolder.DrinkListViewHolder;
import com.nicolasrf.drinkshoprdatabaseserver.Interface.IItemClickListener;
import com.nicolasrf.drinkshoprdatabaseserver.Model.Drink;
import com.nicolasrf.drinkshoprdatabaseserver.R;
import com.nicolasrf.drinkshoprdatabaseserver.UpdateProductActivity;
import com.nicolasrf.drinkshoprdatabaseserver.Utils.Common;
import com.squareup.picasso.Picasso;

import java.util.List;

public class DrinkListAdapter extends RecyclerView.Adapter<DrinkListViewHolder> {

    Context context;
    List<Drink> drinkList;

    public DrinkListAdapter(Context context, List<Drink> drinkList) {
        this.context = context;
        this.drinkList = drinkList;
    }

    @NonNull
    @Override
    public DrinkListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.drink_item_layout,parent,false);
        return new DrinkListViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull DrinkListViewHolder holder, final int position) {

        Picasso.with(context)
                .load(drinkList.get(position).link)
                .into(holder.productImageView);

        holder.priceTextView.setText(new StringBuilder("$").append(String.valueOf(drinkList.get(position).price)).toString());
        holder.drinkNameTextView.setText(drinkList.get(position).name);

        //Event - anti crash null item click
        holder.setItemClickListener(new IItemClickListener() {
            @Override
            public void onClick(View view, boolean isLongClick) {
                Common.currentDrink = drinkList.get(position);
                context.startActivity(new Intent(context, UpdateProductActivity.class));

            }
        });

    }

    @Override
    public int getItemCount() {
        return drinkList.size();
    }
}
