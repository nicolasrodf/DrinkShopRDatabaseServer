package com.nicolasrf.drinkshoprdatabaseserver.Adapter.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.nicolasrf.drinkshoprdatabaseserver.R;

public class OrderDetailViewHolder extends RecyclerView.ViewHolder{

    public ImageView orderItemImageView;
    public TextView drinkNameTextView, drinkAmountTextView, sugarIceTextView, sizeTextView, toppingTextView;

    public OrderDetailViewHolder(@NonNull View itemView) {
        super(itemView);

        orderItemImageView = itemView.findViewById(R.id.order_item_image);
        drinkNameTextView = itemView.findViewById(R.id.drink_name_text_view);
        drinkAmountTextView = itemView.findViewById(R.id.drink_amount_text_view);
        sugarIceTextView = itemView.findViewById(R.id.sugar_ice_text_view);
        sizeTextView = itemView.findViewById(R.id.size_text_view);
        toppingTextView = itemView.findViewById(R.id.topping_text_view);
    }
}