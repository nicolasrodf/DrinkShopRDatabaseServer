package com.nicolasrf.drinkshoprdatabaseserver.Adapter.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.nicolasrf.drinkshoprdatabaseserver.Interface.IItemClickListener;
import com.nicolasrf.drinkshoprdatabaseserver.R;

public class MenuViewHolder  extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener{

    public ImageView productImageView;
    public TextView productTextView;

    IItemClickListener itemClickListener;

    public void setItemClickListener(IItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public MenuViewHolder(@NonNull View itemView) {
        super(itemView);

        productImageView = itemView.findViewById(R.id.product_image_view);
        productTextView = itemView.findViewById(R.id.menu_name_text_view);

        itemView.setOnClickListener(this);
        itemView.setOnLongClickListener(this);
    }

    @Override
    public void onClick(View view) {
        itemClickListener.onClick(view,false);
    }

    @Override
    public boolean onLongClick(View view) {
        itemClickListener.onClick(view,true);
        return true;
    }


}
