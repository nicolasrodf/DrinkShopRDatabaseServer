package com.nicolasrf.drinkshoprdatabaseserver.Adapter.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.nicolasrf.drinkshoprdatabaseserver.Interface.IItemClickListener;
import com.nicolasrf.drinkshoprdatabaseserver.R;

public class DrinkListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView productImageView;
    public TextView drinkNameTextView, priceTextView;

    IItemClickListener itemClickListener;

    public void setItemClickListener(IItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public DrinkListViewHolder(@NonNull View itemView) {
        super(itemView);

        productImageView = itemView.findViewById(R.id.product_image_view);
        drinkNameTextView = itemView.findViewById(R.id.drink_name_text_view);
        priceTextView = itemView.findViewById(R.id.price_text_view);

        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        itemClickListener.onClick(view,false);
    }

}
