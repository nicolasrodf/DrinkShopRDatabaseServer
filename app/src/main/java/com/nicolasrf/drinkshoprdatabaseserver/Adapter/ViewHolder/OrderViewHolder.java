package com.nicolasrf.drinkshoprdatabaseserver.Adapter.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.nicolasrf.drinkshoprdatabaseserver.Interface.IItemClickListener;
import com.nicolasrf.drinkshoprdatabaseserver.R;

public class OrderViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView txt_order_id,txt_order_price,txt_order_address,txt_order_comment,txt_order_status;

    private IItemClickListener itemClickListener;

    public void setItemClickListener(IItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public OrderViewHolder(View itemView) {
        super(itemView);

        txt_order_id = itemView.findViewById(R.id.order_id_text_view);
        txt_order_price = itemView.findViewById(R.id.order_price_text_view);
        txt_order_address = itemView.findViewById(R.id.order_address_text_view);
        txt_order_comment = itemView.findViewById(R.id.order_comment_text_view);
        txt_order_status = itemView.findViewById(R.id.txt_order_status);

        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        itemClickListener.onClick(v,false);
    }

}