package com.nicolasrf.drinkshoprdatabaseserver.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nicolasrf.drinkshoprdatabaseserver.Adapter.ViewHolder.OrderDetailViewHolder;
import com.nicolasrf.drinkshoprdatabaseserver.Model.Cart;
import com.nicolasrf.drinkshoprdatabaseserver.R;
import com.nicolasrf.drinkshoprdatabaseserver.Utils.Common;
import com.squareup.picasso.Picasso;

import java.util.List;

public class OrderDetailAdapter extends RecyclerView.Adapter<OrderDetailViewHolder> {

    Context context;
    List<Cart> itemList;

    //Solo le pasamos el context, em¿n cambio en DrinkApp le pasamos el list el cual armamos en el mismo Order acitivyt (show activity y detail activity)
    public OrderDetailAdapter(Context context) {
        this.context = context;
        this.itemList =  new Gson().fromJson(Common.currentOrder.getOrderDetail(),
                new TypeToken<List<Cart>>() {
                }.getType());
    }

    @NonNull
    @Override
    public OrderDetailViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        View itemView = LayoutInflater.from(context).inflate(R.layout.order_detail_layout,parent,false);
        return new OrderDetailViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderDetailViewHolder holder, int position) {

        holder.drinkAmountTextView.setText(""+itemList.get(position).getAmount());
        holder.drinkNameTextView.setText(new StringBuilder(itemList.get(position).getName()));
        holder.sizeTextView.setText(itemList.get(position).getSize() == 0?"Tamaño M":"Tanaño L");
        holder.sugarIceTextView.setText(new StringBuilder("Azucar: ")
                .append(itemList.get(position).getSugar())
                .append(", Ice: ").append(itemList.get(position).getIce()));
        //check if exist toppings
        if(itemList.get(position).getToppingExtras() != null && !itemList.get(position).getToppingExtras().isEmpty()) {
            String toppingFormat = itemList.get(position).getToppingExtras().replaceAll("\\n", ",");
            toppingFormat = toppingFormat.substring(0, toppingFormat.length() - 1);
            holder.toppingTextView.setText(toppingFormat);
        } else {
            holder.toppingTextView.setText("Sin agregados");
        }

        Picasso.with(context).load(itemList.get(position).getLink()).into(holder.orderItemImageView);

    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }
}
