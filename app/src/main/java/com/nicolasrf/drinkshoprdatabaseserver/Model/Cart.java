package com.nicolasrf.drinkshoprdatabaseserver.Model;

public class Cart {

    //Same as DrinkShopApp (and Order detail declare for OrderDetail in Server)

    private int id;
    private String name;
    private String link;
    private int amount;
    private double price;
    private int sugar;
    private int ice;
    private int size;
    private String toppingExtras;

    public Cart() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getSugar() {
        return sugar;
    }

    public void setSugar(int sugar) {
        this.sugar = sugar;
    }

    public int getIce() {
        return ice;
    }

    public void setIce(int ice) {
        this.ice = ice;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getToppingExtras() {
        return toppingExtras;
    }

    public void setToppingExtras(String toppingExtras) {
        this.toppingExtras = toppingExtras;
    }
}