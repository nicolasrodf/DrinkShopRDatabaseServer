package com.nicolasrf.drinkshoprdatabaseserver;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.nicolasrf.drinkshoprdatabaseserver.Adapter.CategoryAdapter;
import com.nicolasrf.drinkshoprdatabaseserver.Model.Category;
import com.nicolasrf.drinkshoprdatabaseserver.Model.Token;
import com.nicolasrf.drinkshoprdatabaseserver.Utils.Common;
import com.nicolasrf.drinkshoprdatabaseserver.Utils.UploadCallBack;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import id.zelory.compressor.Compressor;
import io.reactivex.disposables.CompositeDisposable;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, UploadCallBack {
    private static final String TAG = "HomeActivity";

    private static final int REQUEST_PERMISSION_CODE = 9999;
    private static final int PICK_FILE_REQUEST = 9998;
    RecyclerView recyclerMenu;
    CompositeDisposable compositeDisposable = new CompositeDisposable();

    EditText nameEditText;
    ImageView browserImageView;

    Uri selectedFileUri = null;
    File compressedImageFile;

    //Uri downloadUri;

    private FirebaseDatabase firebaseDatabase;
    private StorageReference storageReference;

    String lastKey;

    //Ctrl+O
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch(requestCode){
            case REQUEST_PERMISSION_CODE:
            {
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    Toast.makeText(this, "Permission otorgado", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(this, "Permission denegado", Toast.LENGTH_SHORT).show();
            }
            break;
            default:
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle("READY BEBIDAS");

        firebaseDatabase = FirebaseDatabase.getInstance();
        storageReference = FirebaseStorage.getInstance().getReference();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddCategoryDialog();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //View
        recyclerMenu = findViewById(R.id.menu_recycler);
        recyclerMenu.setLayoutManager(new GridLayoutManager(this,2));
        recyclerMenu.setHasFixedSize(true);

        getMenu();

        updateTokenToServer();

        getLastKey();
    }

    private void getLastKey() {
        Query query = firebaseDatabase.getReference("categories").orderByChild("id").limitToLast(1);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        lastKey = child.getKey();
                        Toast.makeText(HomeActivity.this, "LAST KEY: " + lastKey, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void initImageCropper() {
        //Set crop properties.
        CropImage.activity()
                .setInitialCropWindowPaddingRatio(0)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMinCropResultSize(512, 512)
                .setActivityTitle("RECORTAR")
                .setCropMenuCropButtonTitle("OK")
                .start(this);
    }


    private void updateTokenToServer() {
        FirebaseInstanceId.getInstance()
                .getInstanceId()
                .addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
                    @Override
                    public void onSuccess(InstanceIdResult instanceIdResult) {

                        //craate or update token collection

                        Token token = new Token("server_app_01", instanceIdResult.getToken(), "1");

                        firebaseDatabase.getReference("tokens")
                                .child("server_app_01")
                                .setValue(token)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Toast.makeText(HomeActivity.this, "token exitoso", Toast.LENGTH_SHORT).show();
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.d(TAG, "onFailure: ERROR EN MANEJO DE TOKEN " + e.getMessage());
                                Toast.makeText(HomeActivity.this, "Error en ingreso de token", Toast.LENGTH_SHORT).show();
                            }
                        });

                    }

                });
    }



    private void showAddCategoryDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Agregar Nueva Categoría");

        View view = LayoutInflater.from(this).inflate(R.layout.add_category_layout,null);

        nameEditText = view.findViewById(R.id.name_edit_text);
        browserImageView = view.findViewById(R.id.browser_image_view);

        //Event
        browserImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkPermissions();
            }
        });

        //Set View
        builder.setView(view);
        builder.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                selectedFileUri = null;
            }
        }).setPositiveButton("AGREGAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(nameEditText.getText().toString().isEmpty()){
                    Toast.makeText(HomeActivity.this, "Por favor ingrese el nombre de la categoria", Toast.LENGTH_SHORT).show();
                    return;
                } else if(selectedFileUri.toString().isEmpty()){
                    Toast.makeText(HomeActivity.this, "Por favor seleccione la imagen de la categoria", Toast.LENGTH_SHORT).show();
                    return;
                }

                uploadToServerAndDatabase();

            }
        }).show();

    }


    public void checkPermissions(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (ContextCompat.checkSelfPermission(HomeActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                Toast.makeText(HomeActivity.this, "Permission denegado", Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(HomeActivity.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            } else {
                initImageCropper();
            }
        } else {
            initImageCropper();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                selectedFileUri = result.getUri();
                Picasso.with(this).load(selectedFileUri).into(browserImageView);
                //uploadFileToServer(selectedFileUri);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {

                Exception error = result.getError();
                Toast.makeText(HomeActivity.this, "Error crop image.", Toast.LENGTH_SHORT).show();
            }
        }

    }

    private void uploadToServerAndDatabase() {

        if(selectedFileUri != null){

            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading...");
            progressDialog.show();

            //create file
            File file = new File(selectedFileUri.getPath());

            //compress before upload
            try {
                compressedImageFile = new Compressor(HomeActivity.this)
                        .setMaxHeight(640)
                        .setMaxWidth(480)
                        .setQuality(70)
                        .compressToFile(file);
            } catch (IOException e) {
                e.printStackTrace();
            }

            String fileName = UUID.randomUUID().toString();

            //create Uri from file (compressed in this case)
            Uri filePath = Uri.fromFile(compressedImageFile);

            //create reference
            final StorageReference riversRef = storageReference.child("images/category/"+fileName+".jpg");
            //create Upload task for put this file from path
            UploadTask uploadTask = riversRef.putFile(filePath);

            //Hasta aqui ya finaliza la subida, las siguientes son validaciones.-

            uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    // taskSnapshot.getMetadata() contains file metadata such as size, content-type, etc.
                    // ...
                    Toast.makeText(HomeActivity.this, "Uploaded!", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    progressDialog.dismiss();
                    Toast.makeText(HomeActivity.this, "ERROR: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0*taskSnapshot.getBytesTransferred()/taskSnapshot.getTotalByteCount());
                            progressDialog.setMessage("Uploaded: " + (int)progress+"%");
                        }
                    });

            //GET DOWNLOAD URI
            Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }
                    return riversRef.getDownloadUrl();

                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        Uri downloadUri = task.getResult(); //  URI obtenido desde el urlTask! con el metodo del return anterior!
                        Log.d(TAG, "onComplete: DOWNLOAD URI " + downloadUri);

                        final Map<String, Object> category = new HashMap<>();
                        int key = Integer.valueOf(lastKey)+1;
                        category.put("id", String.valueOf(key)); //
                        category.put("name", nameEditText.getText().toString());
                        category.put("link", downloadUri.toString());


                        //
                        firebaseDatabase.getReference("categories")
                                .child(String.valueOf(key))
                                .setValue(category)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Toast.makeText(HomeActivity.this, "Menu agregado", Toast.LENGTH_SHORT).show();
                                        getMenu(); //show menu again (refresh page)
                                        selectedFileUri = null;
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(HomeActivity.this, "Failed: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });

                        //Toast.makeText(HomeActivity.this, "Uri obtenido!", Toast.LENGTH_SHORT).show();
                    } else {
                        // Handle failures
                        // ...
                    }
                }
            });

        }
    }

    private void getMenu() {
        Log.d(TAG, "getMenu: STARTED");

        final List<Category> categories = new ArrayList<>();

        firebaseDatabase.getReference("categories")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                            Category category = snapshot.getValue(Category.class);
                            categories.add(category);
                        }
                        displayMenu(categories);
                        Log.d(TAG, "onDataChange: CATEGORY SIZE " + categories.size());

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

    }

    private void displayMenu(List<Category> categories) {
        Common.menuList = categories;
        CategoryAdapter adapter = new CategoryAdapter(this,categories);
        recyclerMenu.setAdapter(adapter);
    }

    //Ctrl+O

    @Override
    protected void onResume() {
        getMenu();
        isBackButtonClicked = false;
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        compositeDisposable.clear();
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        compositeDisposable.clear();
        super.onStop();
    }

    //Exit App when click Back button
    boolean isBackButtonClicked = false;
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if(isBackButtonClicked){
                super.onBackPressed();
                return;
            }
            this.isBackButtonClicked = true;
            Toast.makeText(this, "Por favor presionar ATRAS para salir de la aplicacion", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_show_order) {
            startActivity(new Intent(HomeActivity.this,ShowOrderActivity.class));

        } else if (id == R.id.nav_message) {
            startActivity(new Intent(HomeActivity.this,SendMessageActivity.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onProgressUpdate(int percentage) {

    }
}
