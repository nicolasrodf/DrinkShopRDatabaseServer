package com.nicolasrf.drinkshoprdatabaseserver;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.nicolasrf.drinkshoprdatabaseserver.Model.Category;
import com.nicolasrf.drinkshoprdatabaseserver.Utils.Common;
import com.nicolasrf.drinkshoprdatabaseserver.Utils.UploadCallBack;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import id.zelory.compressor.Compressor;
import io.reactivex.disposables.CompositeDisposable;

public class UpdateProductActivity extends AppCompatActivity implements UploadCallBack {
    private static final String TAG = "UpdateProductActivity";
    private static final int PICK_FILE_REQUEST = 9999;

    ImageView browserImageView;
    EditText nameEditText, priceEditText, descriptionEditText;
    Button updateButton, deleteButton;

    CompositeDisposable compositeDisposable;

    Uri selectedFileUri = null;
    String selected_category="";
    File compressedImageFile;

    MaterialSpinner menuSpinner;

    HashMap<String,String> menu_data_for_get_key = new HashMap<>();
    HashMap<String,String> menu_data_for_get_value = new HashMap<>();

    List<String> menu_data = new ArrayList<>();

    private FirebaseDatabase firebaseDatabase;
    private StorageReference storageReference;

    //Uri downloadUri;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_product);

        firebaseDatabase = FirebaseDatabase.getInstance();
        storageReference = FirebaseStorage.getInstance().getReference();


        //View
        deleteButton = findViewById(R.id.delete_button);
        updateButton = findViewById(R.id.update_button);
        nameEditText = findViewById(R.id.drink_name_edit_text);
        priceEditText = findViewById(R.id.drink_price_edit_text);
        descriptionEditText = findViewById(R.id.drink_desc_edit_text);
        browserImageView = findViewById(R.id.browser_image_view);
        menuSpinner = findViewById(R.id.menu_id_spinner);

        //RxJava
        compositeDisposable = new CompositeDisposable();

        if(Common.currentDrink != null)
            selected_category = Common.currentCategory.getId();


        //Event
        browserImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkPermissions();
            }
        });

        menuSpinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                selected_category = menu_data_for_get_key.get(menu_data.get(position));
            }
        });

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateProduct();
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteProduct();
            }
        });

        setSpinnerMenu();
        setProductInfo();

    }

    private void setProductInfo() {
        if(Common.currentDrink != null){
            nameEditText.setText(Common.currentDrink.name);
            priceEditText.setText(String.valueOf(Common.currentDrink.price));
            descriptionEditText.setText(Common.currentDrink.description);
            Picasso.with(this).load(Common.currentDrink.link).into(browserImageView);
            menuSpinner.setSelectedIndex(menu_data.indexOf(menu_data_for_get_value.get(Common.currentCategory.getId())));
        }
    }

    private void deleteProduct() {

        //delete product in firestore

        firebaseDatabase.getReference("drinks")
                .child(Common.currentDrink.getId())
                .removeValue()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(UpdateProductActivity.this, "Drink has been deleted", Toast.LENGTH_SHORT).show();
                        selectedFileUri=null;
                        Common.currentDrink=null;
                        finish();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(TAG, "onFailure: " + e.getMessage());
            }
        });


        //Delete from Storage
        // Create a storage reference from our app
        FirebaseStorage storage = FirebaseStorage.getInstance();
        //StorageReference storageRefFull = storage.getReferenceFromUrl(Common.request_selected.getImageFull());
        StorageReference storageRefUrl = storage.getReferenceFromUrl(Common.currentDrink.getLink());

        // Delete the file url
        storageRefUrl.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                // File deleted successfully
                Log.d(TAG, "onSuccess: DELETED IMAGE URL FROM STORAGE");
                finish(); //finish activity
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Uh-oh, an error occurred!
                Log.d(TAG, "onFailure: ERROR: " + exception.getMessage());
                finish(); //finish activity
            }
        });

    }

    private void updateProduct() {

        if(selectedFileUri!=null){

            //Upload to Storage

            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading...");
            progressDialog.show();

            //create file
            File file = new File(selectedFileUri.getPath());

            //compress before upload
            try {
                compressedImageFile = new Compressor(UpdateProductActivity.this)
                        .setMaxHeight(640)
                        .setMaxWidth(480)
                        .setQuality(70)
                        .compressToFile(file);
            } catch (IOException e) {
                e.printStackTrace();
            }

            String fileName = UUID.randomUUID().toString();

            //create Uri from file (compressed in this case)
            Uri filePath = Uri.fromFile(compressedImageFile);

            //create reference
            final StorageReference riversRef = storageReference.child("images/product/"+fileName+".jpg");
            //create Upload task for put this file from path
            UploadTask uploadTask = riversRef.putFile(filePath);

            //Hasta aqui ya finaliza la subida, las siguientes son validaciones.-

            uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    // taskSnapshot.getMetadata() contains file metadata such as size, content-type, etc.
                    // ...
                    Toast.makeText(UpdateProductActivity.this, "Uploaded!", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    progressDialog.dismiss();
                    Toast.makeText(UpdateProductActivity.this, "ERROR: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0*taskSnapshot.getBytesTransferred()/taskSnapshot.getTotalByteCount());
                            progressDialog.setMessage("Uploaded: " + (int)progress+"%");
                        }
                    });

            //GET DOWNLOAD URI
            Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }
                    return riversRef.getDownloadUrl();

                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        Uri downloadUri = task.getResult(); //  URI obtenido desde el urlTask! con el metodo del return anterior!
                        Log.d(TAG, "onComplete: DOWNLOAD URI " + downloadUri);

                        //Update name and link price description

                        Map<String, Object> childUpdates = new HashMap<>();
                        childUpdates.put("name", nameEditText.getText().toString());
                        childUpdates.put("link", downloadUri.toString());
                        childUpdates.put("price", Double.parseDouble(priceEditText.getText().toString()));
                        childUpdates.put("description", descriptionEditText.getText().toString());

                        firebaseDatabase.getReference("drinks")
                                .child(Common.currentDrink.getId())
                                .updateChildren(childUpdates)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Toast.makeText(UpdateProductActivity.this, "Product updated!", Toast.LENGTH_SHORT).show();
                                        selectedFileUri=null;
                                        Common.currentDrink=null;
                                        finish();
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.d(TAG, "onFailure: " + e.getMessage());
                            }
                        });

                        //Toast.makeText(HomeActivity.this, "Uri obtenido!", Toast.LENGTH_SHORT).show();
                    } else {
                        // Handle failures
                        // ...
                    }
                }
            });



        } else {

            //Update only name (link remains with Common.currentCategory.getLink() )

            Map<String, Object> childUpdates = new HashMap<>();
            childUpdates.put("name", nameEditText.getText().toString());
            childUpdates.put("link", Common.currentDrink.getLink());
            childUpdates.put("price", Double.parseDouble(priceEditText.getText().toString()));
            childUpdates.put("description", descriptionEditText.getText().toString());

            firebaseDatabase.getReference("drinks")
                    .child(Common.currentDrink.getId())
                    .updateChildren(childUpdates)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(UpdateProductActivity.this, "Product updated!", Toast.LENGTH_SHORT).show();
                            selectedFileUri=null;
                            Common.currentDrink=null;
                            finish();
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.d(TAG, "onFailure: " + e.getMessage());
                }
            });


        }

        //Si se ha cambiado de categoria en el spinner
        if(!selected_category.equals(Common.currentCategory.getId())){

            //deleteProduct();

            //create Drink Map
            Map<String, Object> childUpdates = new HashMap<>();
            childUpdates.put("menuId", selected_category);

            firebaseDatabase.getReference("drinks")
                    .child(Common.currentDrink.getId())
                    .updateChildren(childUpdates)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(UpdateProductActivity.this, "Drink agregado a nuevo Menu", Toast.LENGTH_SHORT).show();
                            selectedFileUri = null;
                            finish();
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.d(TAG, "onFailure: " + e.getMessage());
                }
            });

        }

    }

    //Crtl+O
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                selectedFileUri = result.getUri();
                Picasso.with(this).load(selectedFileUri).into(browserImageView);
                //uploadFileToServer(selectedFileUri);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {

                Exception error = result.getError();
                Toast.makeText(UpdateProductActivity.this, "Error crop image.", Toast.LENGTH_SHORT).show();
            }
        }

    }

    private void initImageCropper() {
        //Set crop properties.
        CropImage.activity()
                .setInitialCropWindowPaddingRatio(0)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMinCropResultSize(512, 512)
                .setActivityTitle("RECORTAR")
                .setCropMenuCropButtonTitle("OK")
                .start(this);
    }

    public void checkPermissions(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (ContextCompat.checkSelfPermission(UpdateProductActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                Toast.makeText(UpdateProductActivity.this, "Permiso denegado", Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(UpdateProductActivity.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            } else {
                initImageCropper();
            }
        } else {
            initImageCropper();
        }
    }


    private void setSpinnerMenu() {
        for(Category category : Common.menuList){ //Common.menuList lo seteamos = categories en HomeActivity
            menu_data_for_get_key.put(category.getName(),category.getId());
            menu_data_for_get_value.put(category.getId(),category.getName());

            menu_data.add(category.getName());

            menuSpinner.setItems(menu_data);
        }
    }

    @Override
    public void onProgressUpdate(int percentage) {

    }
}
